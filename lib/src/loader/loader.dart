import 'package:flutter/material.dart';
import 'package:flutter_common/errors.dart';
import 'package:flutter_common/src/loader/item_bloc.dart';

typedef LoaderBuilder<T extends ItemDataState> = Widget Function(BuildContext, T);

class LoaderWidget<S extends ItemDataState> extends StatelessWidget {
  final ItemBloc loader;
  final LoaderBuilder<S> builder;
  final Widget Function(BuildContext)? loadingBuilder;
  final Widget Function(BuildContext)? errorBuilder;

  const LoaderWidget(
      {Key? key,
      required this.loader,
      required this.builder,
      this.loadingBuilder,
      this.errorBuilder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<ItemState>(
        stream: loader.stream(),
        initialData: loader.init(),
        builder: (context, snapshot) {
          if (snapshot.data is S) {
            final S state = snapshot.data as S;
            return builder(context, state);
          } else if (snapshot.data is ItemErrorState) {
            final ItemErrorState? state = snapshot.data as ItemErrorState?;
            if (state!.error is ErrorEx) {
              WidgetsBinding.instance!.addPostFrameCallback((_) {
                showError(context, state.error as ErrorEx);
              });
            } else {
              throw state.error;
            }
            return errorBuilder != null ? errorBuilder!(context) : _reload();
          }
          return loadingBuilder != null
              ? loadingBuilder!(context)
              : const Center(child: CircularProgressIndicator());
        });
  }

  Widget _reload() {
    return Center(
        child: IconButton(tooltip: 'Reload', icon: const Icon(Icons.sync), onPressed: loader.load));
  }
}
