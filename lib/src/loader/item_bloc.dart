import 'dart:async';

abstract class ItemState {}

class ItemInitState extends ItemState {}

class ItemLoadingState extends ItemState {}

class ItemErrorState extends ItemState {
  ItemErrorState(this.error);

  final Object error;
}

abstract class ItemDataState<T> extends ItemState {
  ItemDataState(this.data);

  final T data;
}

abstract class ItemBloc {
  final _itemStreamController = StreamController<ItemState>.broadcast();
  ItemState _init = ItemInitState();

  ItemBloc();

  ItemState init() {
    return _init;
  }

  Stream<ItemState> stream() {
    return _itemStreamController.stream;
  }

  void load() {
    publishState(loading());
    loadDataRoute().then((value) {
      publishState(value);
    }, onError: (error) {
      _handleError(error);
    });
  }

  void dispose() {
    _itemStreamController.close();
  }

  ItemLoadingState loading() {
    return ItemLoadingState();
  }

  Future<ItemDataState> loadDataRoute();

  void publishState(ItemState state) {
    _itemStreamController.add(state);
    _init = state;
  }

  void _handleError(Object error) {
    publishState(ItemErrorState(error));
  }
}
