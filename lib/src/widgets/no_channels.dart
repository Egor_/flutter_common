import 'package:flutter/material.dart';

class NonAvailableBuffer extends StatelessWidget {
  final String? message;
  final IconData? icon;
  final double iconSize;
  final double textSize;
  final double opacity;
  final Color? color;

  const NonAvailableBuffer(
      {this.icon,
      this.iconSize = 96,
      this.message,
      this.textSize = 20,
      this.opacity = 0.5,
      this.color});

  @override
  Widget build(BuildContext context) {
    final content = message == null
        ? const SizedBox()
        : Text(message!,
            style: TextStyle(fontSize: textSize, color: color),
            textAlign: TextAlign.center,
            softWrap: true);
    return Opacity(
        opacity: opacity,
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Icon(icon, size: iconSize, color: color),
          SizedBox(height: iconSize / 4),
          content
        ]));
  }
}
