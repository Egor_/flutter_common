import 'package:flutter/material.dart';

class PlayerButtons extends IconButton {
  const PlayerButtons.play({VoidCallback? onPressed, Color? color})
      : super(icon: const Icon(Icons.play_arrow), color: color, onPressed: onPressed);

  const PlayerButtons.pause({VoidCallback? onPressed, Color? color})
      : super(icon: const Icon(Icons.pause), color: color, onPressed: onPressed);

  const PlayerButtons.previous({VoidCallback? onPressed, Color? color})
      : super(icon: const Icon(Icons.skip_previous), color: color, onPressed: onPressed);

  const PlayerButtons.next({VoidCallback? onPressed, Color? color})
      : super(icon: const Icon(Icons.skip_next), color: color, onPressed: onPressed);

  const PlayerButtons.seekBackward({VoidCallback? onPressed, Color? color})
      : super(icon: const Icon(Icons.replay_5), color: color, onPressed: onPressed);

  const PlayerButtons.seekForward({VoidCallback? onPressed, Color? color})
      : super(icon: const Icon(Icons.forward_5), color: color, onPressed: onPressed);
}
