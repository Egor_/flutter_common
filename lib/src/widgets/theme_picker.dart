

import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';

class ColorPickerDialog extends StatefulWidget {
  final String title;
  final Color initColor;
  final String submit;
  final String cancel;

  const ColorPickerDialog(
      {this.title = 'Color picker',
      required this.initColor,
      this.submit = 'Submit',
      this.cancel = 'Cancel'});

  @override
  _ColorPickerDialogState createState() {
    return _ColorPickerDialogState();
  }
}

class _ColorPickerDialogState extends State<ColorPickerDialog> {
  late Color tempShadeColor;

  @override
  void initState() {
    super.initState();
    tempShadeColor = widget.initColor;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        contentPadding: const EdgeInsets.all(6.0),
        title: Text(widget.title),
        content: _content(),
        actions: [_cancel(), _submit()]);
  }

  Widget _content() {
    return MaterialColorPicker(
        shrinkWrap: true,
        selectedColor: tempShadeColor,
        onColorChange: (color) => tempShadeColor = color);
  }

  Widget _cancel() {
    return TextButton(
        child: Text(widget.cancel),
        onPressed: () {
          Navigator.of(context).pop();
        });
  }

  Widget _submit() {
    return ElevatedButton(
        child: Text(widget.submit),
        onPressed: () {
          Navigator.of(context).pop(tempShadeColor);
        });
  }
}
