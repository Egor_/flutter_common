import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class Clock extends StatefulWidget {
  final int type;
  final double dateFontSize;
  final double timeFontSize;
  final Color? textColor;
  final bool hour24;

  const Clock.date(
      {this.dateFontSize = 18, this.timeFontSize = 24, this.textColor, this.hour24 = true})
      : type = 1;

  const Clock.time(
      {this.dateFontSize = 18, this.timeFontSize = 24, this.textColor, this.hour24 = true})
      : type = 2;

  const Clock.full(
      {this.dateFontSize = 18, this.timeFontSize = 24, this.textColor, this.hour24 = true})
      : type = 3;

  @override
  _ClockState createState() {
    return _ClockState();
  }
}

class _ClockState extends State<Clock> {
  late String _timeString;
  late String _dateString;
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    initializeDateFormatting();
    _updateDate();
    _timer = Timer.periodic(const Duration(minutes: 1), _getCurrentTime);
  }

  @override
  void didUpdateWidget(Clock oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.hour24 != widget.hour24) {
      _updateDate();
    }
  }

  @override
  Widget build(BuildContext context) {
    final baseStyle = TextStyle(color: widget.textColor);
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          if (widget.type != 1)
            Text(_timeString, style: baseStyle.copyWith(fontSize: widget.timeFontSize)),
          if (widget.type != 2)
            Text(_dateString, style: baseStyle.copyWith(fontSize: widget.dateFontSize))
        ]);
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _getCurrentTime(_) {
    setState(() {
      _updateDate();
    });
  }

  void _updateDate() {
    final dateTime = DateTime.now();
    final timeFormat = widget.hour24 ? DateFormat.HOUR24_MINUTE : DateFormat.HOUR_MINUTE;
    _timeString = DateFormat(timeFormat).format(dateTime);
    _dateString = DateFormat('E, MMM d').format(dateTime);
  }
}
