import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class FlatButtonEx extends StatelessWidget {
  final bool filled;
  final String text;
  final VoidCallback? onPressed;

  const FlatButtonEx.filled({required this.text, this.onPressed}) : filled = true;

  const FlatButtonEx.notFilled({required this.text, this.onPressed}) : filled = false;

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
        padding: const EdgeInsets.all(8),
        child: filled
            ? ElevatedButtonEx(text: text, onPressed: onPressed)
            : TextButtonEx(text: text, onPressed: onPressed));
  }
}

class ElevatedButtonEx extends ElevatedButton {
  ElevatedButtonEx({required String text, required VoidCallback? onPressed})
      : super(onPressed: onPressed, child: Text(text));

  factory ElevatedButtonEx.icon(
      {required String text, required IconData icon, required VoidCallback onPressed}) {
    return ElevatedButton.icon(onPressed: onPressed, icon: Icon(icon), label: Text(text)) as ElevatedButtonEx;
  }
}

class OutlinedButtonEx extends OutlinedButton {
  OutlinedButtonEx({required String text, required VoidCallback onPressed})
      : super(onPressed: onPressed, child: Text(text));

  factory OutlinedButtonEx.icon(
      {required String text, required IconData icon, required VoidCallback onPressed}) {
    return OutlinedButton.icon(onPressed: onPressed, icon: Icon(icon), label: Text(text)) as OutlinedButtonEx;
  }
}

class TextButtonEx extends TextButton {
  TextButtonEx({required String text, required VoidCallback? onPressed})
      : super(onPressed: onPressed, child: Text(text));

  factory TextButtonEx.icon(
      {required String text, required IconData icon, required VoidCallback onPressed}) {
    return TextButton.icon(onPressed: onPressed, icon: Icon(icon), label: Text(text)) as TextButtonEx;
  }
}
