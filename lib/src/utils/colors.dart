import 'package:flutter/material.dart';

bool isColorLight(Color color) {
  return ThemeData.estimateBrightnessForColor(color) == Brightness.light;
}

bool isThemeLight(BuildContext context) {
  return Theme.of(context).brightness == Brightness.light;
}

Color themeBrightnessColor(BuildContext context,
    {Color onDark = Colors.white, Color onLight = Colors.black}) {
  return isThemeLight(context) ? onLight : onDark;
}

Color backgroundColorBrightness(Color color,
    {Color onDark = Colors.white, Color onLight = Colors.black}) {
  return isColorLight(color) ? onLight : onDark;
}
