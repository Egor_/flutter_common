import 'package:flutter/material.dart';

class ErrorEx extends Object {
  final String? errorDescription;

  ErrorEx(this.errorDescription);
}

class ErrorExHttp extends ErrorEx {
  final int statusCode;
  final String? reason;

  ErrorExHttp(this.statusCode, this.reason, [String? errorDescription]) : super(errorDescription);
}

Future showError(BuildContext context, ErrorEx error) {
  late String title;
  if (error is ErrorExHttp) {
    title = '${error.statusCode} ${error.reason}';
  } else {
    title = 'Error';
  }

  Widget? content;
  if (error.errorDescription != null) {
    content = Text(error.errorDescription!, softWrap: true);
  }

  return showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(title: Text(title), content: content);
      });
}
